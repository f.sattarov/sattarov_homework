import java.util.Scanner;
import java.util.Arrays;
public class number1{
	private static final Scanner userInput = new Scanner(System.in);
	private static int arraySize=0;
	private static int[] array;

	public static void main(String[] args) {
		System.out.println("Enter size of array: ");
		arraySize = userInput.nextInt();
		array=new int [arraySize];

		for (int i=0; i<arraySize; i++){
			System.out.println("Enter" + i + "elements of array:");
			array[i] = userInput.nextInt();
		}
		int [] resultnumber=number(array);
		System.out.println("Number:");
		printArray(resultnumber);
		System.out.println();
		int resultSumOfArray=sumOfArray(array);
		System.out.println("Sum of array is:" + resultSumOfArray);
		int [] resultOfReverse = reverseArray(array);
		System.out.println("Reverse array:");
		printArray(resultOfReverse);
		System.out.println();
		int [] resultswap=swap(array);
		System.out.println("Swap:");
		printArray(resultswap);
		System.out.println();
		int [] resultbubblesort = bubblesort(array);
		System.out.println("Bubble sort:");
		printArray(resultbubblesort);
		System.out.println();
		int resultAverageOfArray=average(array);
		System.out.println("Average:" + resultAverageOfArray);


	}
		public static int sumOfArray(int[] arr){
			int sumOfArray = 0;
			for(int i=0; i<=arraySize-1;i++){
				sumOfArray=sumOfArray+array[i];
				}
					return sumOfArray;
		}

		public static int [] reverseArray(int [] arr){
			int [] resultArray = new int[arr.length];
			int i = 0;
			for (int j=arr.length-1; j>=0;j--, i++){
				 resultArray[i]=arr[j];
			}
			return resultArray;

	}
		public static int [] swap(int [] arr){
			int max=0;
			int min=0;
			int temp;
		for ( int i = 0; i < arr.length; i++ ) {
            if ( arr[min] > arr[i] ) min = i;
            if ( arr[max] < arr[i] ) max = i;
		}
		temp = array[min];
        array[min] = array[max];
        array[max] = temp;
        return arr;
    
		}
		public static int [] number(int [] arr){
		int [] arraynumber = new int[arr.length];
		int number = 0;
		int j=0;
		for (int i=0; i<arr.length;i++,j++){
			number=number*10+arr[i];
			arraynumber[j]=arr[i];
		}
		return arraynumber;
		}
		public static int [] bubblesort(int []arr){
		
		for(int i = arr.length-1 ; i > 0 ; i--){
        for(int j = 0 ; j < i ; j++){
            if( arr[j] > arr[j+1] ){
                int temp = arr[j];
                arr[j] = arr[j+1];
                arr[j+1] = temp;

               }

           }

        }
    return arr;
   }
   public static int average(int [] arr){
   	int i = 0;
	int sum = 0;

		while (i < arr.length) {
			sum = sum + arr[i];
			i++;
		}

		int average = sum / arr.length;
	
   	 return average;
   
   }
		public static void printArray(int [] arrayToPrint){
			for (int i=0; i<arrayToPrint.length; i++){
				System.out.printf("<" + arrayToPrint[i]+ "> ");
		}
	}

}